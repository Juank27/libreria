import { createRouter, createWebHistory } from 'vue-router'
import App from './App.vue'
import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import Home from './components/Home.vue'
import Account from './components/Account.vue'
import SubirLibros from './components/SubirLibros.vue'


// Modificar rutas segun proyecto Libreria----cargar libros

const routes = [{
   path: '/',
   name: 'root',
   component: App
},

{
  path: '/user/logIn',
  name: "logIn",
  component: LogIn
},
{
  path: '/user/signUp',
  name: "signUp",
  component: SignUp
},
{
  path: '/user/SubirLibros',
  name: "SubirLibros",
  component: SubirLibros
},

{
  path: '/home',
  name: "home",
  component: Home
},

{
  path: '/user/account',
  name: "account",
  component: Account
},


]

const router = createRouter({
   history: createWebHistory(),
   routes
})

export default router

