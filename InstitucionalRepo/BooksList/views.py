from django.shortcuts import render

# Creando las vistas de presentación

from rest_framework import viewsets
from BooksList.models import InputBooks
from BooksList.serializers import BooksModelSerializer

class BooksViewSet (viewsets.ModelViewSet):
    queryset = InputBooks.objects.all()
    serializer_class = BooksModelSerializer
