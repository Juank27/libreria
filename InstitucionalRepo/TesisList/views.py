from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from TesisList.models import InputTesis
from TesisList.serializers import TesisModelSerializer

class TesisViewSet (viewsets.ModelViewSet):
    queryset = InputTesis.objects.all()
    serializer_class = TesisModelSerializer
